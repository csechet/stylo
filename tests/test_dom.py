from typing import cast

from cssselect2.tree import ElementWrapper

from stylo.dom import FOCUS, HOVER, DomState


def test_contains() -> None:
    node = cast(ElementWrapper, object())

    state = DomState()
    assert state in DomState()
    assert state in DomState((node, HOVER))
    assert state in DomState((node, HOVER | FOCUS))

    state = DomState((node, HOVER))
    assert state not in DomState()
    assert state in DomState((node, HOVER))
    assert state in DomState((node, HOVER | FOCUS))

    state = DomState((node, HOVER | FOCUS))
    assert state not in DomState()
    assert state not in DomState((node, HOVER))
    assert state not in DomState((node, FOCUS))
    assert state in DomState((node, HOVER | FOCUS))

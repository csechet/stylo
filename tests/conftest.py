import os
from typing import NoReturn

import pytest

if os.getenv("_PYTEST_RAISE", "0") != "0":

    @pytest.hookimpl(tryfirst=True)  # type: ignore
    def pytest_exception_interact(call) -> NoReturn:
        raise call.excinfo.value

    @pytest.hookimpl(tryfirst=True)  # type: ignore
    def pytest_internalerror(excinfo) -> NoReturn:
        raise excinfo.value

from cssselect2.parser import parse as parse_selector
from cssselect2.tree import ElementWrapper
from html5lib import parse as parse_html

from stylo.dom import FOCUS, HOVER, DomState
from stylo.selector import compile_selector, get_matching_state


def test_get_matching_states() -> None:
    root = ElementWrapper.from_html_root(
        parse_html(
            """
            <html>
              <body>
                <ul class="list">
                  <li class="item">
                    <a class="link"></a>
                    <div class="description"></div>
                    <a class="link test"></a>
                  </li>
                </ul>
              </body>
            </html>
        """
        )
    )

    def _state(selector: str) -> DomState:
        parsed_selector = next(parse_selector(selector))
        node = root.query(compile_selector(parsed_selector))
        return get_matching_state(parsed_selector, node)

    item = root.query(".item")
    link = root.query(".link")

    assert _state(".item:hover .link") == DomState((item, HOVER))
    assert _state(".item:hover > .link") == DomState((item, HOVER))
    assert _state(".link:hover + .description") == DomState((link, HOVER))
    assert _state(".link:hover ~ .description") == DomState((link, HOVER))

    assert _state(".item:hover > .link:focus") == DomState((item, HOVER), (link, FOCUS))
    assert _state(".item:hover:focus") == DomState((item, HOVER | FOCUS))

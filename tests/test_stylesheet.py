from io import StringIO

from cssselect2.tree import ElementWrapper
from html5lib import parse

from stylo.dom import HOVER, DomState
from stylo.nodes import Declaration
from stylo.stylesheet import Stylesheet


def _load(html: str, css: str) -> tuple[ElementWrapper, Stylesheet]:
    element_wrapper = ElementWrapper.from_html_root(parse(html))
    return element_wrapper, Stylesheet(StringIO(css))


def _assert_style_equals(actual: dict[str, Declaration], expected: dict[str, str]) -> None:
    actual_dict = {it.name: it.value for it in actual.values()}
    assert actual_dict == expected


def test_match_simple_selector() -> None:
    root, stylesheet = _load(
        "<html><body><p class='test-class'></p></body>",
        ".test-class { }",
    )
    paragraph = root.query("p")
    matches = list(stylesheet.match(paragraph))
    assert len(matches) == 1
    selector, _ = matches[0]
    assert str(selector) == ".test-class"


def test_match_selector_pseudo_class() -> None:
    root, stylesheet = _load(
        "<html><body><p class='test-class'></p></body>",
        ".test-class:hover { }",
    )
    paragraph = root.query("p")
    matches = list(stylesheet.match(paragraph))
    assert len(matches) == 1
    selector, _ = matches[0]
    assert str(selector) == ".test-class:hover"


def test_style_pseudo_class() -> None:
    root, stylesheet = _load(
        "<html><body><a class='link'></body></html>",
        """
        .link { background: blue; color: red;}
        .link:hover { background: red; }
        """,
    )
    link = root.query("a")
    styled_node = stylesheet.style(link)
    normal_style = styled_node.get_style(DomState())

    _assert_style_equals(normal_style, {"background": "blue", "color": "red"})

    hover_style = styled_node.get_style(DomState((link, HOVER)))
    _assert_style_equals(hover_style, {"background": "red", "color": "red"})

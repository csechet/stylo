from pathlib import Path
from typing import Any

from stylo.filters.overlaps import Overlap, OverlapType, get_overlaps


def test_overlaps(shared_datadir: Path) -> None:
    page_path = shared_datadir / "page.html"
    css_path = shared_datadir / "style.css"

    def _get_content(overlap: Overlap) -> tuple[str, str, OverlapType]:
        return (str(overlap.selector), str(overlap.overlapping_selector), overlap.type)

    def _(selector: Any) -> list[tuple[str, str, OverlapType]]:
        return list(
            map(
                _get_content,
                get_overlaps(selector, f"file://{page_path}", [css_path]),
            )
        )

    assert _(".list--item") == [
        (".list--item", ".list>div", OverlapType.CONTAINS),
        (".list--item a", ".list>div a", OverlapType.CONTAINS),
    ]
    assert _(".list--link") == [
        (".list--link", ".list--item>a", OverlapType.EQUALS),
        (".list--link", ".list a", OverlapType.CONTAINED),
        (".list--link", ".list>div a", OverlapType.INTERSECTS),
    ]

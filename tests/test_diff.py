from io import StringIO

from stylo.diff import DiffItem, diff
from stylo.page_set import PageSet
from stylo.stylesheet import Stylesheet


def _diff(html: str, left: str, right: str) -> list[DiffItem]:
    page = PageSet([StringIO(html)])
    left_stylesheet = Stylesheet(StringIO(left))
    right_stylesheet = Stylesheet(StringIO(right))
    return list(diff(page, left_stylesheet, right_stylesheet))


def test_diff_add_property() -> None:
    delta = _diff(
        "<html><body><p class='test-class'></p></body>",
        ".test-class { color: blue; }",
        ".test-class { color: blue; background: blue; }",
    )

    assert len(delta) == 1

    node, left, right = delta[0]
    assert node.local_name == "p"
    assert left is None
    assert right is not None
    assert right.name == "background"
    assert right.value == "blue"


def test_diff_change_property() -> None:
    delta = _diff(
        "<html><body><p class='test-class'></p></body>",
        ".test-class { color: blue; }",
        ".test-class { color: black; }",
    )

    assert len(delta) == 1

    node, left, right = delta[0]
    assert left is not None
    assert node.local_name == "p"
    assert left.name == "color"
    assert left.value == "blue"
    assert right is not None
    assert right.name == "color"
    assert right.value == "black"


def test_diff_remove_property() -> None:
    delta = _diff(
        "<html><body><p class='test-class'></p></body>",
        ".test-class { color: blue; }",
        ".test-class { }",
    )

    assert len(delta) == 1

    node, left, right = delta[0]
    assert node.local_name == "p"
    assert left is not None
    assert left.name == "color"
    assert left.value == "blue"
    assert right is None

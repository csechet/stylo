from typing import Iterable

from cssselect2 import ElementWrapper

from stylo.nodes import Declaration
from stylo.page_set import PageSet
from stylo.stylesheet import Stylesheet

DiffItem = tuple[ElementWrapper, Declaration | None, Declaration | None]


def diff(page_set: PageSet, left: Stylesheet, right: Stylesheet) -> Iterable[DiffItem]:
    for element in page_set.elements:
        left_styled_node = left.style(element)
        right_styled_node = right.style(element)

        states = set(left_styled_node.matching_dom_states) | set(right_styled_node.matching_dom_states)

        for state in states:
            left_style = left_styled_node.get_style(state)
            right_style = right_styled_node.get_style(state)
            left_declarations = set(left_style.keys())
            right_declarations = set(right_style.keys())

            for declaration in left_declarations - right_declarations:
                yield (element, left_style[declaration], None)

            for declaration in left_declarations & right_declarations:
                left_declaration = left_style[declaration]
                right_declaration = right_style[declaration]
                if str(left_declaration) != str(right_declaration):
                    yield (element, left_style[declaration], right_style[declaration])

            for declaration in right_declarations - left_declarations:
                yield (element, None, right_style[declaration])

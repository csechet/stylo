"""Access CSS and source HTML for themes."""
from enum import Enum
from itertools import product
from logging import getLogger
from pathlib import Path
from typing import Any, Iterable

from cssselect2 import ElementWrapper
from cssselect2.compiler import CompiledSelector
from cssselect2.parser import CombinedSelector, Selector, SelectorError
from cssselect2.parser import parse as css_parse
from tinycss2.ast import QualifiedRule

from stylo.page import Page

_LOG = getLogger(__name__)


class OverlapType(Enum):
    EQUALS = 1
    CONTAINS = 2
    CONTAINED = 3
    INTERSECTS = 4


class Overlap:
    def __init__(  # pylint: disable=too-many-arguments
        self,
        page: Page,
        rule: QualifiedRule,
        selector: CombinedSelector,
        overlapping_selector: CombinedSelector,
        type_: OverlapType,
    ):
        self._page = page
        self._rule = rule
        self._selector = selector
        self._overlapping_selector = overlapping_selector
        self._type = type_

    @property
    def page(self) -> Page:
        return self._page

    @property
    def rule(self) -> QualifiedRule:
        return self._rule

    @property
    def selector(self) -> CombinedSelector:
        return self._selector

    @property
    def overlapping_selector(self) -> CombinedSelector:
        return self._overlapping_selector

    @property
    def type(self) -> OverlapType:
        return self._type


def _parse(content: Any) -> Iterable[CombinedSelector]:
    return list(it.parsed_tree for it in css_parse(content, forgiving=True))


def _query(page: Page, selector: CombinedSelector) -> set[ElementWrapper]:
    compiled_selector = CompiledSelector(Selector(selector))
    return set(page.root.query_all(compiled_selector))


def get_overlaps(base_selector: str, url: str, stylesheets: Iterable[Path]) -> Iterable[Overlap]:
    compiled_selectors = _parse(base_selector)
    pages = Page.load(url, stylesheets)
    for selector, page in product(compiled_selectors, pages):
        yield from _get_selector_overlaps(selector, page)


def _get_selector_overlaps(selector: CombinedSelector, page: Page) -> Iterable[Overlap]:
    matching_nodes = _query(page, selector)
    for rule in page.qualified_rules:
        for left, combinator, right in _get_rule_overlaps(matching_nodes, rule, page):

            if str(left) == str(selector):
                continue

            if right is None:
                original_selector = selector
                overlapping_selector = left
            else:
                original_selector = _combine(selector, combinator, right)
                overlapping_selector = _combine(left, combinator, right)

            original_nodes = _query(page, original_selector)
            overlapping_nodes = _query(page, overlapping_selector)

            if not original_nodes and not overlapping_nodes:
                continue

            if original_nodes == overlapping_nodes:
                overlap_type = OverlapType.EQUALS
            elif original_nodes.issuperset(overlapping_nodes):
                overlap_type = OverlapType.CONTAINS
            elif overlapping_nodes.issuperset(original_nodes):
                overlap_type = OverlapType.CONTAINED
            else:
                assert original_nodes & overlapping_nodes
                overlap_type = OverlapType.INTERSECTS

            yield Overlap(page, rule, original_selector, overlapping_selector, overlap_type)


def _match_any(selector: CombinedSelector, *nodes: ElementWrapper) -> bool:
    compiled_selector = CompiledSelector(Selector(selector))
    return any(it.matches(compiled_selector) for it in nodes)


def _combine(left: CombinedSelector, combinator: str, right: CombinedSelector | None) -> CombinedSelector:
    if right is None:
        return left

    if not isinstance(right, CombinedSelector):
        return CombinedSelector(left, combinator, right)

    return _combine(CombinedSelector(left, combinator, right.left), right.combinator, right.right)


def _get_rule_overlaps(
    matching_nodes: set[ElementWrapper], rule: QualifiedRule, page: Page
) -> Iterable[CompiledSelector]:
    for selector in _parse(rule.prelude):
        left = selector
        combinator: str = ""
        right: CombinedSelector | None = None
        right_left_leaf: CombinedSelector | None = None

        while True:

            try:
                if _match_any(left, *matching_nodes):
                    yield left, combinator, right
                    break
            except SelectorError as ex:
                position = page.get_position(rule)
                _LOG.warning(
                    "%s:%i:%i : Error parsing selector : %s",
                    position.source,
                    position.line,
                    position.column,
                    ex,
                )
                break

            if not isinstance(left, CombinedSelector):
                break

            if right is None:
                right = left.right
            elif right_left_leaf is None:
                right = CombinedSelector(left.right, combinator, right)
                right_left_leaf = right
            else:
                right_left_leaf.left = CombinedSelector(left.right, combinator, right_left_leaf.left)
                right_left_leaf = right_left_leaf.left
            combinator = left.combinator
            left = left.left

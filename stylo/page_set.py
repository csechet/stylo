from pathlib import Path
from typing import IO, Iterable
from urllib.parse import urlparse
from urllib.request import urlopen

from cssselect2 import ElementWrapper
from html5lib import parse


def _load_page(url: str) -> ElementWrapper:
    with urlopen(url) as page_content:
        return ElementWrapper.from_html_root(parse(page_content))


PageSource = str | Path | IO[str]


def _load_content(source: PageSource) -> ElementWrapper:
    if isinstance(source, str) and urlparse(source).scheme != "":
        with urlopen(source) as content:
            return content

    if isinstance(source, (Path, str)):
        with open(source, "r", encoding="utf-8") as file:
            return file.read()

    return source.read()


class PageSet:
    def __init__(self, sources: Iterable[PageSource]):
        self._roots = list(
            map(
                ElementWrapper.from_html_root,
                map(parse, map(_load_content, sources)),
            )
        )

    @property
    def elements(self) -> Iterable[ElementWrapper]:
        for root in self._roots:
            yield from root.iter_subtree()

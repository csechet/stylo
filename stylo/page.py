"""Access CSS and source HTML for themes."""
from pathlib import Path
from typing import Iterable
from urllib.request import urlopen

from cssselect2 import ElementWrapper
from html5lib import parse as parse_html
from tinycss2 import parse_rule_list, parse_stylesheet
from tinycss2.ast import AtRule, Node, QualifiedRule

from stylo.source_map import SourceMap, SourcePosition


class Page:
    def __init__(
        self,
        stylesheet_path: Path,
        root: ElementWrapper,
        rules: Iterable[Node],
        source_map: SourceMap | None = None,
    ) -> None:
        self._stylesheet_path = stylesheet_path
        self._root = root
        self._rules = list(rules)
        self._source_map = source_map

    @staticmethod
    def load(url: str, stylesheets: Iterable[Path]) -> "Iterable[Page]":
        with urlopen(url) as page_content:
            page_root = ElementWrapper.from_html_root(parse_html(page_content))

        for stylesheet_path in stylesheets:
            with open(stylesheet_path, "r", encoding="utf-8") as stylesheet:
                content = stylesheet.read()
                rules = parse_stylesheet(content, skip_whitespace=True, skip_comments=True)
                source_map = SourceMap.load(stylesheet_path)
                yield Page(stylesheet_path, page_root, rules, source_map)

    @property
    def root(self) -> ElementWrapper:
        return self._root

    @property
    def rules(self) -> Iterable[Node]:
        return self._rules

    @property
    def qualified_rules(self) -> Iterable[QualifiedRule]:
        def _expand(rule: Node) -> Iterable[QualifiedRule]:
            if isinstance(rule, AtRule):
                if rule.at_keyword != "media":
                    return
                rules = parse_rule_list(rule.content, skip_comments=True, skip_whitespace=True)
                yield from [it for it in rules if isinstance(it, QualifiedRule)]
            elif isinstance(rule, QualifiedRule):
                yield rule

        for rule in self.rules:
            yield from _expand(rule)

    def get_position(self, node: Node) -> SourcePosition:
        if self._source_map is None:
            return SourcePosition(self._stylesheet_path, node.source_line, node.source_column)
        return self._source_map[node.source_line, node.source_column]

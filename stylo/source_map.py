"""Decode source maps"""
from abc import ABC, abstractmethod
from bisect import bisect
from json import loads
from pathlib import Path
from re import compile as re_compile
from typing import Final, Iterable, Optional


class SourcePosition:
    def __init__(self, source: Path, line: int, column: int) -> None:
        self._source = source
        self._line = line
        self._column = column

    @property
    def source(self) -> Path:
        return self._source

    @property
    def line(self) -> int:
        return self._line

    @property
    def column(self) -> int:
        return self._column

    def __str__(self) -> str:
        return f"{self._source}:{self._line}:{self._column}"


class SourceMap(ABC):
    @staticmethod
    def load(source_path: Path) -> "SourceMap":
        source_directory = source_path.parent
        source_map_path = _get_map_path(source_path)
        if source_map_path is None or not source_map_path.is_file():
            return _NullSourceMap(source_path)

        with open(source_map_path, "r", encoding="utf-8") as source_map:
            json_map = loads(source_map.read())

        if json_map["version"] != 3:
            raise ValueError("Only version 3 sourcemaps are supported")

        sources = list((source_directory / it).resolve() for it in json_map["sources"])
        mappings = json_map.get("mappings", "")

        index, bisect_index = _load_indices(sources, mappings)
        return _JsonSourceMap(index, bisect_index)

    @abstractmethod
    def __getitem__(self, key: tuple[int, int]) -> SourcePosition:
        pass


class _NullSourceMap(SourceMap):
    def __init__(self, source: Path) -> None:
        self._source = source

    def __getitem__(self, key: tuple[int, int]) -> SourcePosition:
        return SourcePosition(self._source, key[0], key[1])


PositionIndex = dict[tuple[int, int], SourcePosition]
BisectIndex = list[list[int]]


class _JsonSourceMap(SourceMap):
    def __init__(
        self,
        index: PositionIndex,
        bisect_index: BisectIndex,
    ):
        self._index = index
        self._bisect_index = bisect_index

    def __getitem__(self, key: tuple[int, int]) -> SourcePosition:
        result = self._index.get(key, None)
        if result is not None:
            return result

        line, column = key

        columns = self._bisect_index[line]
        column_index = bisect(columns, column)
        column = columns[column_index and column_index - 1]
        return self._index[line, column]


B64CHARS: Final[bytes] = b"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
B64TABLE: Final[list[Optional[int]]] = [None] * (max(B64CHARS) + 1)
for i, b in enumerate(B64CHARS):
    B64TABLE[b] = i

SHIFTSIZE: Final[int] = 5
FLAG: Final[int] = 1 << 5
MASK: Final[int] = (1 << 5) - 1


def _base64vlq_decode(vlqval: str) -> Iterable[int]:
    shift = value = 0
    for v in map(B64TABLE.__getitem__, vlqval.encode("ascii")):
        value += (v & MASK) << shift  # type: ignore  # v is always int
        if v & FLAG:  # type: ignore  # v is always int
            shift += SHIFTSIZE
            continue
        # determine sign and add to results
        yield (value >> 1) * (-1 if value & 1 else 1)
        shift = value = 0


def _get_map_path(source_path: Path) -> Path | None:
    if not source_path.is_file():
        return None

    map_url_regex = re_compile(r"\/[\/|\*][@|#]\s*sourceMappingURL=([^\*\/]*)\s*(\*\/)?")

    with open(source_path, "r", encoding="utf-8") as source:
        for line in source.readlines():
            match = map_url_regex.match(line)
            if match:
                source_map_path = match.group(1).strip()
                return source_path.parent / source_map_path

    return None


def _load_indices(  # pylint: disable = too-many-locals
    sources: list[Path], mappings: str
) -> tuple[PositionIndex, BisectIndex]:
    index = {}
    bisect_index = []
    source_id = source_line = source_column = 0

    for target_line, vlqs in enumerate(mappings.split(";")):
        columns = []
        target_column = 0

        if vlqs:
            for target_column_offset, *args in map(_base64vlq_decode, vlqs.split(",")):
                target_column += target_column_offset
                if len(args) >= 3:
                    (
                        source_id_offset,
                        source_line_offset,
                        source_column_offset,
                        *_,
                    ) = args
                    source_id += source_id_offset
                    source_line += source_line_offset
                    source_column += source_column_offset

                index[(target_line, target_column)] = SourcePosition(
                    sources[source_id], source_line, source_column
                )
                columns.append(target_column)

        bisect_index.append(columns)

    return index, bisect_index

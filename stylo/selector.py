from typing import Any, TypeVar

from cssselect2 import ElementWrapper
from cssselect2.compiler import CompiledSelector
from cssselect2.parser import CombinedSelector, CompoundSelector, PseudoClassSelector, Selector

from stylo.dom import DomState, get_pseudo_class_flag

TSelector = TypeVar("TSelector")


def compile_selector(selector: Any) -> CompiledSelector:
    if isinstance(selector, Selector):
        tree = selector.parsed_tree
        pseudo_element = selector.pseudo_element
    else:
        assert isinstance(selector, (CombinedSelector, CompoundSelector))
        tree = selector
        pseudo_element = None

    stripped_tree = _strip_dynamic_pseudo_classes(tree)
    return CompiledSelector(Selector(stripped_tree, pseudo_element))


def get_matching_state(selector: Selector, node: ElementWrapper) -> DomState:
    state: dict[ElementWrapper, int] = {}
    tree = selector.parsed_tree
    while tree:
        node_state = _get_matching_state(tree)

        if node_state != 0:
            state[node] = node_state

        if not isinstance(tree, CombinedSelector):
            break

        node, tree = _get_parent_match(node, tree)

    return DomState(*state.items())


def _get_matching_state(tree: Any) -> int:
    if isinstance(tree, CombinedSelector):
        tree = tree.right

    state: int = 0

    if isinstance(tree, CompoundSelector):
        for selector in tree.simple_selectors:
            if not isinstance(selector, PseudoClassSelector):
                continue

            state |= get_pseudo_class_flag(selector.name)

    return state


def _get_parent_match(node: ElementWrapper, tree: CombinedSelector) -> tuple[ElementWrapper, Any]:
    combinator = tree.combinator

    if combinator == ">":
        node = node.parent
    elif combinator == "+":
        node = node.previous
    elif combinator == " ":
        left = compile_selector(tree.left)
        while not node.matches(left):
            node = node.parent
    elif combinator == "~":
        left = compile_selector(tree.left)
        for previous in node.previous_siblings:
            if previous.matches(left):
                node = previous
                break

    return node, tree.left


def _strip_dynamic_pseudo_classes(tree: Any) -> Any:
    if isinstance(tree, CompoundSelector):
        return CompoundSelector(
            [tree for tree in tree.simple_selectors if not isinstance(tree, PseudoClassSelector)]
        )
    if isinstance(tree, CombinedSelector):
        return CombinedSelector(
            _strip_dynamic_pseudo_classes(tree.left),
            tree.combinator,
            _strip_dynamic_pseudo_classes(tree.right),
        )

    assert False

from functools import cached_property
from logging import getLogger
from typing import Any, Generic, Iterable, TypeVar, cast

from cssselect2.parser import Selector, parse
from tinycss2 import serialize
from tinycss2.ast import Declaration as TCSSDeclaration
from tinycss2.ast import Node as TCSSNode
from tinycss2.ast import ParseError as TCSSParseError
from tinycss2.ast import QualifiedRule as TCSSQualifiedRule
from tinycss2.ast import WhitespaceToken as TCSSWhitespaceToken
from tinycss2.parser import parse_declaration_list

from stylo.source_map import SourceMap, SourcePosition

TWrapped = TypeVar("TWrapped", bound=TCSSNode)

_LOG = getLogger(__name__)


def _check_error(node: TCSSNode, source_map: SourceMap) -> bool:
    position = source_map[node.source_line, node.source_column]
    if isinstance(node, TCSSParseError):
        _LOG.warning("%s: %s", position, node.message)
        return False
    return True


class Node(Generic[TWrapped]):
    def __init__(self, wrapped_node: TWrapped, source_map: SourceMap):
        self._source_map = source_map
        self._wrapped_node = wrapped_node

    @cached_property
    def source_position(self) -> SourcePosition:
        return self._source_map[self._wrapped_node.source_line, self._wrapped_node.source_column]

    @staticmethod
    def wrap(nodes: Iterable[TWrapped], source_map: SourceMap) -> "Iterable[Node[TWrapped]]":
        for node in nodes:
            if not _check_error(node, source_map):
                continue

            if isinstance(node, TCSSQualifiedRule):
                yield QualifiedRule(node, source_map)
            elif isinstance(node, TCSSWhitespaceToken):
                yield Whitespace(node, source_map)
            else:
                assert False

    def __str__(self) -> str:
        return cast(str, serialize([self._wrapped_node]))


class Whitespace(Node[TCSSWhitespaceToken]):
    pass


class Declaration(Node[TCSSDeclaration]):
    @property
    def name(self) -> str:
        return cast(str, self._wrapped_node.lower_name)

    @property
    def value(self) -> str:
        return cast(str, serialize(self._wrapped_node.value)).strip()

    @property
    def important(self) -> bool:
        return cast(bool, self._wrapped_node.important)


class QualifiedRule(Node[TCSSQualifiedRule]):
    @cached_property
    def selectors(self) -> Iterable[Selector]:
        return cast(Iterable[Any], parse(self._wrapped_node.prelude))

    @cached_property
    def declarations(self) -> Iterable[Declaration]:
        def _list() -> Iterable[Declaration]:
            for declaration in parse_declaration_list(self._wrapped_node.content):
                if not _check_error(declaration, self._source_map):
                    continue
                if isinstance(declaration, TCSSWhitespaceToken):
                    continue
                yield Declaration(declaration, self._source_map)

        return list(_list())

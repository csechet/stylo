"""frontools main module"""
from logging import INFO, basicConfig, getLogger
from pathlib import Path
from typing import Iterable

from click import Path as ClickPath
from click import argument, group

from stylo.filters.overlaps import OverlapType, get_overlaps

_LOG = getLogger(__file__)


@group()
def main() -> None:
    """Utilities for EO frontend development."""
    basicConfig(format="%(message)s", level=INFO)


_OVERLAP_TYPE_TEXT = {
    OverlapType.EQUALS: "EQUALS",
    OverlapType.CONTAINS: "CONTAINS",
    OverlapType.CONTAINED: "IS CONTAINED BY",
    OverlapType.INTERSECTS: "INTERSECTS",
}


@main.command(name="overlaps")
@argument("selector", type=str)
@argument("url", type=str, required=True)
@argument("stylesheets", type=ClickPath(exists=True), nargs=-1)
def list_aliases(selector: str, url: str, stylesheets: Iterable[str]) -> None:
    """Find all selectors overlapping SELECTOR applying STYLESHEETS to URL content."""
    stylesheet_paths = [Path(it) for it in stylesheets]
    for overlap in get_overlaps(selector, url, stylesheet_paths):
        position = overlap.page.get_position(overlap.rule)
        _LOG.info(
            "%s:%i:%i %s %s %s",
            position.source,
            position.line,
            position.column,
            overlap.selector,
            _OVERLAP_TYPE_TEXT[overlap.type],
            overlap.overlapping_selector,
        )


if __name__ == "__main__":
    main()  # pylint: disable=no-value-for-parameter

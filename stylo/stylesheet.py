from functools import cached_property, lru_cache
from pathlib import Path
from typing import IO, Iterable, cast

from cssselect2 import ElementWrapper, Matcher
from cssselect2.parser import Selector
from tinycss2 import parse_stylesheet

from stylo.dom import DomState
from stylo.nodes import Declaration, Node, QualifiedRule
from stylo.selector import compile_selector, get_matching_state
from stylo.source_map import SourceMap

Match = tuple[Selector, QualifiedRule]


class StyledNode:
    def __init__(self, node: ElementWrapper, matches: Iterable[Match]) -> None:
        self._node = node
        self._matches = list(matches)

    @cached_property
    def matching_dom_states(self) -> set[DomState]:
        return set(get_matching_state(selector, self._node) for selector, _ in self._matches)

    def get_style(self, state: DomState) -> dict[str, Declaration]:
        declarations: dict[str, Declaration] = {}
        for selector, rule in self._matches:
            if get_matching_state(selector, self._node) not in state:
                continue

            for declaration in rule.declarations:
                name = declaration.name
                previous_declaration = declarations.get(name, None)

                if (
                    previous_declaration is not None
                    and previous_declaration.important
                    and not declaration.important
                ):
                    continue

                declarations[name] = declaration

        return declarations


class Stylesheet:
    def __init__(self, content: IO[str]) -> None:
        path = Path(getattr(content, "name", "unknown"))
        source_map = SourceMap.load(path)

        self._nodes = Node.wrap(parse_stylesheet(content.read()), source_map)
        self._matcher = Matcher()
        for rule in self.qualified_rules:
            for selector in rule.selectors:
                self._matcher.add_selector(compile_selector(selector), (selector, rule))

    @cached_property
    def qualified_rules(self) -> Iterable[QualifiedRule]:
        def _list() -> Iterable[QualifiedRule]:
            for node in self._nodes:
                if isinstance(node, QualifiedRule):
                    yield node

        return list(_list())

    def match(self, node: ElementWrapper) -> Iterable[Match]:
        for match in self._matcher.match(node):
            selector, rule = match[3]
            yield cast(Selector, selector), cast(QualifiedRule, rule)

    @lru_cache
    def style(self, node: ElementWrapper) -> StyledNode:
        return StyledNode(node, self.match(node))

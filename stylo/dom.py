from cssselect2 import ElementWrapper

HOVER = 0x01
FOCUS = 0x02


_PSEUDO_CLASSES_FLAGS = {
    "hover": HOVER,
    "focus": FOCUS,
}


def get_pseudo_class_flag(pseudo_class: str) -> int:
    return _PSEUDO_CLASSES_FLAGS.get(pseudo_class, 0)


class DomState:
    def __init__(self, *args: tuple[ElementWrapper, int]) -> None:
        self._hash: int | None = None
        self._element_states = dict(args)

    def __eq__(self, right: object) -> bool:
        if not isinstance(right, DomState):
            return False
        return self._element_states == right._element_states

    def __contains__(self, other: "DomState") -> bool:
        for element, element_state in other._element_states.items():
            included_element_state = self._element_states.get(element, 0)
            if (element_state & included_element_state) != element_state:
                return False

        return True

    def __hash__(self) -> int:
        if self._hash is None:
            self._hash = frozenset(self._element_states.items()).__hash__()
        return self._hash

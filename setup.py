#!/usr/bin/python
"""stylo setup."""
from pathlib import Path

from setuptools import setup

setup(
    name="stylo",
    description="",
    long_description=(Path(__file__).parent / "README.md").read_text(),
    long_description_content_type="text/markdown",
    keywords=["front-end"],
    packages=[
        "stylo",
    ],
    entry_points={
        "console_scripts": [
            "stylo=stylo.__main__:main",
        ],
    },
    license="GPL",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.8",
    ],
    install_requires=["click", "tinycss2", "cssselect2", "html5lib"],
    extras_require={
        "dev": [
            "nox",
            "pytest",
            "pytest-datadir",
        ]
    },
    author="Entr'ouvert",
    author_email="csechet@entrouvert.com",
    zip_safe=False,
    setuptools_git_versioning={
        "enabled": True,
    },
)
